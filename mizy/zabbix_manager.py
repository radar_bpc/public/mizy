import getpass
import hashlib
from pyzabbix import ZabbixAPI


class ZabbixAPIManager:
    def __init__(self, url, user, password=None):
        self.zabbix_client = ZabbixAPI(url)
        self._user = user
        self._password = (
            password if password else getpass.getpass("Enter Zabbix password: ")
        )
        self._hashed_password = hashlib.sha256(self._password.encode()).hexdigest()
        self.connect()

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, value):
        self._user = value

    @property
    def password(self):
        raise ValueError("Password retrieval is not allowed.")

    @password.setter
    def password(self, value):
        self._password = value
        self._hashed_password = hashlib.sha256(self._password.encode()).hexdigest()

    def connect(self):
        self.zabbix_client.login(self._user, self._password)

    def list_hosts(self):
        hosts = self.zabbix_client.host.get(output="extend")
        for h in hosts:
            print(h["hostid"], h["name"])
        return hosts

    def get_host_details(self, host_id):
        host_details = self.zabbix_client.host.get(output="extend", hostids=host_id)
        for detail in host_details:
            print(detail)

    def create_host(self, ip, hostname):
        default_group_id = "1"
        default_template_id = "10001"

        self.zabbix_client.host.create(
            host=hostname,
            interfaces=[
                {
                    "type": 1,
                    "main": 1,
                    "useip": 1,
                    "ip": ip,
                    "dns": "",
                    "port": "10050",
                }
            ],
            groups=[{"groupid": default_group_id}],
            templates=[{"templateid": default_template_id}],
        )

    def delete_host(self, host_id):
        try:
            delete_response = self.zabbix_client.host.delete(host_id)
            print(f"Deleted host with ID: {host_id}")
        except Exception as e:
            print(f"Error deleting host: {e}")

    # Item Management Methods
    def list_items(self, host_id):
        items = self.zabbix_client.item.get(output="extend", hostids=host_id)
        for i in items:
            print(i["itemid"], i["name"])
        return items

    def get_item_details(self, item_id):
        item_details = self.zabbix_client.item.get(output="extend", itemids=item_id)
        for detail in item_details:
            print(detail)

    def create_item(self, host_id, key, name, value_type=0):
        self.zabbix_client.item.create(
            hostid=host_id,
            key_=key,
            name=name,
            type=2,  # Zabbix trapper
            value_type=value_type  # Numeric float by default
        )

    def delete_item(self, item_id):
        try:
            self.zabbix_client.item.delete(item_id)
            print(f"Deleted item with ID: {item_id}")
        except Exception as e:
            print(f"Error deleting item: {e}")

    # Trigger Management Methods
    def list_triggers(self, host_id):
        triggers = self.zabbix_client.trigger.get(output="extend", hostids=host_id)
        for t in triggers:
            print(t["triggerid"], t["description"])
        return triggers

    def get_trigger_details(self, trigger_id):
        trigger_details = self.zabbix_client.trigger.get(
            output="extend", triggerids=trigger_id
        )
        for detail in trigger_details:
            print(detail)

    def create_trigger(self, expression, description, priority=4):
        self.zabbix_client.trigger.create(
            expression=expression, description=description, priority=priority
        )

    def delete_trigger(self, trigger_id):
        try:
            self.zabbix_client.trigger.delete(trigger_id)
            print(f"Deleted trigger with ID: {trigger_id}")
        except Exception as e:
            print(f"Error deleting trigger: {e}")

    # Event Management Methods
    def list_events(self, trigger_id):
        events = self.zabbix_client.event.get(output="extend", triggerids=trigger_id)
        for e in events:
            print(e["eventid"], e["name"])
        return events

    def get_event_details(self, event_id):
        event_details = self.zabbix_client.event.get(output="extend", eventids=event_id)
        for detail in event_details:
            print(detail)

    # Alert Management Methods
    def list_alerts(self):
        alerts = self.zabbix_client.alert.get(output="extend")
        if len(alerts) == 0:
            response = "No recent alerts."
            print(response)
            return response
        for a in alerts:
            print(a["alertid"], a["subject"], a["message"])
        return alerts

    def get_alert_details(self, alert_id):
        alert_details = self.zabbix_client.alert.get(output="extend", alertids=alert_id)
        for detail in alert_details:
            print(detail)

    # Graph Management Methods
    def list_graphs(self, host_id):
        graphs = self.zabbix_client.graph.get(output="extend", hostids=host_id)
        for g in graphs:
            print(g["graphid"], g["name"])
        return graphs

    def get_graph_details(self, graph_id):
        graph_details = self.zabbix_client.graph.get(output="extend", graphids=graph_id)
        for detail in graph_details:
            print(detail)

    def create_graph(self, name, items, graphtype=0, width=900, height=200):
        self.zabbix_client.graph.create(
            name=name,
            width=width,
            height=height,
            gitems=items,
            graphtype=graphtype
        )
        print(f"Graph {name} created.")

    def delete_graph(self, graph_id):
        try:
            self.zabbix_client.graph.delete(graph_id)
            print(f"Deleted graph with ID: {graph_id}")
        except Exception as e:
            print(f"Error deleting graph: {e}")

    # User Management Methods
    def list_users(self):
        users = self.zabbix_client.user.get(output="extend")
        for u in users:
            print(u["userid"], u["alias"])
        return users

    def get_user_details(self, user_id):
        user_details = self.zabbix_client.user.get(output="extend", userids=user_id)
        for detail in user_details:
            print(detail)

    def create_user(self, alias, passwd, usrgrps):
        usergroups = [{"usrgrpid": grp} for grp in usrgrps]
        self.zabbix_client.user.create(
            alias=alias,
            passwd=passwd,
            usrgrps=usergroups
        )

    def delete_user(self, user_id):
        self.zabbix_client.user.delete(user_id)

    # Group Management Methods
    def list_groups(self):
        groups = self.zabbix_client.hostgroup.get(output="extend")
        for g in groups:
            print(g["groupid"], g["name"])
        return groups

    def get_group_details(self, group_id):
        group_details = self.zabbix_client.hostgroup.get(output="extend", groupids=group_id)
        for detail in group_details:
            print(detail)

    def create_group(self, name):
        self.zabbix_client.hostgroup.create(name=name)

    def delete_group(self, group_id):
        try:
            self.zabbix_client.hostgroup.delete(group_id)
            print(f"Deleted group with ID: {group_id}")
        except Exception as e:
            print(f"Error deleting group: {e}")

    # Template Management Methods
    def list_templates(self):
        templates = self.zabbix_client.template.get(output="extend")
        for t in templates:
            print(t["templateid"], t["name"])
        return templates

    def get_template_details(self, template_id):
        template_details = self.zabbix_client.template.get(output="extend", templateids=template_id)
        for detail in template_details:
            print(detail)

    def create_template(self, name, group_ids):
        formatted_group_ids = [{"groupid": gid} for gid in group_ids]
        self.zabbix_client.template.create(host=name, groups=formatted_group_ids)

    def delete_template(self, template_id):
        try:
            self.zabbix_client.template.delete(template_id)
            print(f"Deleted template with ID: {template_id}")
        except Exception as e:
            print(f"Error deleting template: {e}")

    # Maintenance Management Methods
    def start_maintenance(self, name, host_ids, active_since, active_till):
        self.zabbix_client.maintenance.create(
            name=name,
            active_since=active_since,
            active_till=active_till,
            hostids=host_ids,
            timeperiods=[
                {
                    "timeperiod_type": 0,  # One-off maintenance
                    "start_date": active_since,
                    "period": active_till - active_since,
                }
            ],
        )

    def end_maintenance(self, maintenance_id):
        self.zabbix_client.maintenance.delete(maintenance_id)

    def list_maintenances(self):
        maintenances = self.zabbix_client.maintenance.get(output="extend")
        for m in maintenances:
            print(m["maintenanceid"], m["name"], m["active_since"], m["active_till"])
