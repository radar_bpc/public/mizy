import argparse
from .zabbix_manager import ZabbixAPIManager


class CLIManager:
    def __init__(self):
        self.parser = argparse.ArgumentParser(description="Zabbix CLI Utility")
        self.subparsers = self.parser.add_subparsers(title="commands", dest="command")

    def setup_parser(self):
        host_parser = self.subparsers.add_parser("host", help="Manage hosts")
        host_subparsers = host_parser.add_subparsers(dest="host_command")

        host_get = host_subparsers.add_parser("get", help="Get host info")
        host_get.add_argument("--host_id", required=True, help="Host ID")

        host_list = host_subparsers.add_parser("list", help="List all hosts")

        host_create = host_subparsers.add_parser("create", help="Create a new host")
        host_create.add_argument("--ip", required=True, help="IP address of the host")
        host_create.add_argument(
            "--hostname", required=True, help="Hostname of the host"
        )

        host_delete = host_subparsers.add_parser("delete", help="Delete host")
        host_delete.add_argument("--host_id", required=True, help="Host ID to delete")

        # Item Subparsers
        item_parser = self.subparsers.add_parser("item", help="Manage items")
        item_subparsers = item_parser.add_subparsers(dest="item_command")

        item_get = item_subparsers.add_parser("get", help="Get item info")
        item_get.add_argument("--item_id", required=True, help="Item ID")

        item_list = item_subparsers.add_parser("list", help="List all items for a host")
        item_list.add_argument(
            "--host_id", required=True, help="Host ID for which to list items"
        )
        item_create = item_subparsers.add_parser("create", help="Create a new item")
        item_create.add_argument(
            "--host_id", required=True, help="Host ID where the item will be created"
        )
        item_create.add_argument("--key", required=True, help="Key for the item")
        item_create.add_argument("--name", required=True, help="Name of the item")
        item_create.add_argument(
            "--value_type",
            choices=[0, 1, 2, 3, 4],
            default=0,
            type=int,
            help="Type of value the item will hold",
        )
        item_delete = item_subparsers.add_parser("delete", help="Delete item")
        item_delete.add_argument("--item_id", required=True, help="Item ID to delete")

        # Trigger Subparsers
        trigger_parser = self.subparsers.add_parser("trigger", help="Manage triggers")
        trigger_subparsers = trigger_parser.add_subparsers(dest="trigger_command")

        trigger_get = trigger_subparsers.add_parser("get", help="Get trigger info")
        trigger_get.add_argument("--trigger_id", required=True, help="Trigger ID")

        trigger_list = trigger_subparsers.add_parser(
            "list", help="List all triggers for a host"
        )
        trigger_list.add_argument(
            "--host_id", required=True, help="Host ID for which to list triggers"
        )
        trigger_create = trigger_subparsers.add_parser(
            "create", help="Create a new trigger"
        )
        trigger_create.add_argument(
            "--expression", required=True, help="Trigger expression"
        )
        trigger_create.add_argument(
            "--description", required=True, help="Description of the trigger"
        )
        trigger_create.add_argument(
            "--priority",
            type=int,
            choices=[0, 1, 2, 3, 4, 5],
            default=4,
            help="Priority of the trigger",
        )
        trigger_delete = trigger_subparsers.add_parser("delete", help="Delete trigger")
        trigger_delete.add_argument(
            "--trigger_id", required=True, help="Trigger ID to delete"
        )

        # Event Subparsers
        event_parser = self.subparsers.add_parser("event", help="Manage events")
        event_subparsers = event_parser.add_subparsers(dest="event_command")

        event_get = event_subparsers.add_parser("get", help="Get event info")
        event_get.add_argument("--event_id", required=True, help="Event ID")

        event_list = event_subparsers.add_parser(
            "list", help="List all events for a trigger"
        )
        event_list.add_argument(
            "--trigger_id", required=True, help="Trigger ID for which to list events"
        )

        # Alert Subparsers
        alert_parser = self.subparsers.add_parser("alert", help="Manage alerts")
        alert_subparsers = alert_parser.add_subparsers(dest="alert_command")

        alert_get = alert_subparsers.add_parser("get", help="Get alert info")
        alert_get.add_argument("--alert_id", required=True, help="Alert ID")

        alert_list = alert_subparsers.add_parser("list", help="List all alerts")

        # Graph Subparsers
        graph_parser = self.subparsers.add_parser("graph", help="Manage graphs")
        graph_subparsers = graph_parser.add_subparsers(dest="graph_command")

        # Create
        graph_create = graph_subparsers.add_parser("create", help="Create a new graph")
        graph_create.add_argument("--name", required=True, help="Name of the graph")
        graph_create.add_argument(
            "--width", type=int, default=900, help="Width of the graph"
        )
        graph_create.add_argument(
            "--height", type=int, default=200, help="Height of the graph"
        )
        graph_create.add_argument("--item_id", required=True, help="Item ID")
        graph_create.add_argument(
            "--items",
            nargs="+",
            required=True,
            help="List of item ids to associate with the graph",
        )
        graph_list = graph_subparsers.add_parser("list", help="List all graphs")
        graph_list.add_argument(
            "--host_id", required=True, help="Host ID for which to list graphs"
        )
        graph_get = graph_subparsers.add_parser("get", help="Get graph details")
        graph_get.add_argument("--graph_id", required=True, help="Graph ID to retrieve")

        graph_delete = graph_subparsers.add_parser("delete", help="Delete graph")
        graph_delete.add_argument(
            "--graph_id", required=True, help="Graph ID to delete"
        )

        # User Subparsers
        user_parser = self.subparsers.add_parser("user", help="Manage users")
        user_subparsers = user_parser.add_subparsers(dest="user_command")

        user_get = user_subparsers.add_parser("get", help="Get user info")
        user_get.add_argument("--user_id", required=True, help="User ID")

        user_list = user_subparsers.add_parser("list", help="List all users")

        user_create = user_subparsers.add_parser("create", help="Create a new user")
        user_create.add_argument("--alias", required=True, help="Alias of the user")
        user_create.add_argument(
            "--passwd", required=True, help="Password for the user"
        )
        user_create.add_argument(
            "--usrgrps",
            nargs="+",
            required=True,
            type=str,
            help="User group IDs for the user (space-separated)",
        )
        user_delete = user_subparsers.add_parser("delete", help="Delete user")
        user_delete.add_argument("--user_id", required=True, help="User ID to delete")

        # Group Subparsers
        group_parser = self.subparsers.add_parser("group", help="Manage host groups")
        group_subparsers = group_parser.add_subparsers(dest="group_command")

        group_get = group_subparsers.add_parser("get", help="Get host group info")
        group_get.add_argument("--group_id", required=True, help="Group ID")

        group_list = group_subparsers.add_parser("list", help="List all host groups")

        group_create = group_subparsers.add_parser(
            "create", help="Create a new host group"
        )
        group_create.add_argument(
            "--name", required=True, help="Name of the host group"
        )

        group_delete = group_subparsers.add_parser("delete", help="Delete host group")
        group_delete.add_argument(
            "--group_id", required=True, help="Group ID to delete"
        )

        # Template Subparsers
        template_parser = self.subparsers.add_parser(
            "template", help="Manage templates"
        )
        template_subparsers = template_parser.add_subparsers(dest="template_command")

        template_get = template_subparsers.add_parser("get", help="Get template info")
        template_get.add_argument("--template_id", required=True, help="Template ID")

        template_list = template_subparsers.add_parser(
            "list", help="List all templates"
        )

        template_create = template_subparsers.add_parser(
            "create", help="Create a new template"
        )
        template_create.add_argument(
            "--name", required=True, help="Name of the template"
        )
        template_create.add_argument(
            "--group_ids",
            nargs="+",
            required=True,
            help="Group IDs the template belongs to",
        )

        template_delete = template_subparsers.add_parser(
            "delete", help="Delete template"
        )
        template_delete.add_argument(
            "--template_id", required=True, help="Template ID to delete"
        )

        # Maintenance Subparsers
        maintenance_parser = self.subparsers.add_parser(
            "maintenance", help="Manage maintenances"
        )
        maintenance_subparsers = maintenance_parser.add_subparsers(
            dest="maintenance_command"
        )
        maintenance_start = maintenance_subparsers.add_parser(
            "start", help="Start a maintenance"
        )
        maintenance_start.add_argument(
            "--name", required=True, help="Name of the maintenance"
        )
        maintenance_start.add_argument(
            "--host_ids",
            nargs="+",
            required=True,
            help="List of host IDs for the maintenance",
        )
        maintenance_start.add_argument(
            "--active_since",
            type=int,
            required=True,
            help="Maintenance start time as UNIX timestamp",
        )
        maintenance_start.add_argument(
            "--active_till",
            type=int,
            required=True,
            help="Maintenance end time as UNIX timestamp",
        )
        maintenance_end = maintenance_subparsers.add_parser(
            "end", help="End a maintenance"
        )
        maintenance_end.add_argument(
            "--maintenance_id", required=True, help="Maintenance ID to end"
        )
        maintenance_list = maintenance_subparsers.add_parser(
            "list", help="List all maintenances"
        )


def main():
    cli_manager = CLIManager()
    cli_manager.setup_parser()
    args = cli_manager.parser.parse_args()

    zapi_manager = ZabbixAPIManager("http://localhost:8080", "Admin", "zabbix")

    commands = {
        'host': {
            'get': (zapi_manager.get_host_details, ["host_id"]),
            'list': (zapi_manager.list_hosts, []),
            'create': (zapi_manager.create_host, ["ip", "hostname"]),
            'delete': (zapi_manager.delete_host, ["host_id"])
        },
        'item': {
            'get': (zapi_manager.get_item_details, ["item_id"]),
            'list': (zapi_manager.list_items, ["host_id"]),
            'create': (zapi_manager.create_item, ["host_id", "key", "name", "value_type"]),
            'delete': (zapi_manager.delete_item, ["item_id"])
        },
        'trigger': {
            'get': (zapi_manager.get_trigger_details, ["trigger_id"]),
            'list': (zapi_manager.list_triggers, ["host_id"]),
            'create': (zapi_manager.create_trigger, ["expression", "description", "priority"]),
            'delete': (zapi_manager.delete_trigger, ["trigger_id"])
        },
        'event': {
            'get': (zapi_manager.get_event_details, ["event_id"]),
            'list': (zapi_manager.list_events, ["trigger_id"]),
        },
        'alert': {
            'get': (zapi_manager.get_alert_details, ["alert_id"]),
            'list': (zapi_manager.list_alerts, []),
        },
        'graph': {
            'list': (zapi_manager.list_graphs, ["host_id"]),
            'get': (zapi_manager.get_graph_details, ["graph_id"]),
            'create': (zapi_manager.create_graph, ["name", "width", "height", "item_id", "items"]),
            'delete': (zapi_manager.delete_graph, ["graph_id"]),
        },
        'user': {
            'get': (zapi_manager.get_user_details, ["user_id"]),
            'list': (zapi_manager.list_users, []),
            'create': (zapi_manager.create_user, ["alias", "passwd", "usrgrps"]),
            'delete': (zapi_manager.delete_user, ["user_id"]),
        },
        'group': {
            'get': (zapi_manager.get_group_details, ["group_id"]),
            'list': (zapi_manager.list_groups, []),
            'create': (zapi_manager.create_group, ["name"]),
            'delete': (zapi_manager.delete_group, ["group_id"]),
        },
        'template': {
            'get': (zapi_manager.get_template_details, ["template_id"]),
            'list': (zapi_manager.list_templates, []),
            'create': (zapi_manager.create_template, ["name", "group_ids"]),
            'delete': (zapi_manager.delete_template, ["template_id"]),
        },
        'maintenance': {
            'start': (zapi_manager.start_maintenance, ["name", "host_ids", "active_since", "active_till"]),
            'end': (zapi_manager.end_maintenance, ["maintenance_id"]),
            'list': (zapi_manager.list_maintenances, []),
        }
    }

    sub_command = getattr(args, f"{args.command}_command", None)
    command_details = commands.get(args.command, {}).get(sub_command)
    if command_details:
        func, params = command_details
        param_values = [getattr(args, param) for param in params]
        func(*param_values)


if __name__ == "__main__":
    main()
