# Mizy - Zabbix CLI Utility

Mizy provides a command-line interface for managing various Zabbix components. With Mizy, administrators can interact with Zabbix directly from the command line, simplifying repetitive tasks and streamlining monitoring management.

## Features

- Manage Hosts: List, create, retrieve details, and delete hosts.
- Handle Items: List, create, get information, and remove items.
- Triggers: Set up, list, view, and delete triggers.
- Events & Alerts: View events and alerts.
- Graph Management: Create, list, fetch details, and erase graphs.
- User Operations: CRUD operations for Zabbix users.
- Group Tasks: Handle host groups.
- Template Management: Oversee Zabbix templates.
- Maintenance: Start, end, and list maintenances.

## Prerequisites

- Python 3.x
- Installed Zabbix server

## Installation

1. Clone the repository:

```bash
git clone https://github.com/yourusername/mizy.git
cd mizy
```

2. Set up a virtual environment and install the required packages using Poetry:

```bash
python -m venv env
source ./env/bin/activate  # Use ".\env\Scripts\Activate.ps1" on Windows for PowerShell
pip install poetry
poetry install
```

## Usage

Here's a basic overview of the usage. For detailed options, use the `--help` flag with any command.

```bash
# List all hosts
mizy host list

# Create a new host
mizy host create --ip 192.168.1.1 --hostname Server1

# Retrieve details about an item
mizy item get --item_id 101

# Start a maintenance window
mizy maintenance start --name "Maintenance Window 1" --host_ids 101 102 --active_since 1640995200 --active_till 1641002400
```

## Using Docker for Testing

For ease of testing, we've provided a `docker-compose.yml` to set up a Zabbix test environment complete with a PostgreSQL database, Zabbix server, and Zabbix web interface.

### Requirements:

- Docker
- Docker Compose

### Steps:

1. From the root directory of the project, run:
```bash
docker-compose up -d
```

This will start up a PostgreSQL server, a Zabbix server connected to it, and the Zabbix web interface. The Zabbix web interface can be accessed at `http://localhost:8080/` with the default login `Admin` and password `Admin`.

2. To shut down the environment and remove the containers, run:
```bash
docker-compose down
```

## Testing

Mizy is equipped with a comprehensive test suite to ensure the reliability and accuracy of its functionalities.

### Running Tests

To run tests, make sure you're in the project directory and have activated the Poetry environment. Then, execute:

```bash
poetry run pytest
```

This will run the test suite and display the results. It's a good practice to run tests after making any changes to the codebase to ensure everything still works as expected.

## License

This project utilizes `pyzabbix`, which is licensed under LGPLv3. Adherence to LGPLv3 terms is maintained where the `pyzabbix` library is used.
