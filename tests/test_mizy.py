import io
import pytest
from contextlib import redirect_stdout

from mizy import cli


def run_script(args_list):
    import sys
    sys.argv = ["script"] + args_list
    f = io.StringIO()
    with redirect_stdout(f):
        cli.main()
    return f.getvalue().strip()


def test_list_hosts():
    host_list_output = run_script(["host", "list"])
    host_list_lines = host_list_output.splitlines()
    zabbix_server_line = [line for line in host_list_lines if "Zabbix server" in line]
    assert zabbix_server_line

def test_get_host_details():
    host_list_output = run_script(["host", "list"])
    host_list_lines = host_list_output.splitlines()
    zabbix_server_id = [line.split()[0] for line in host_list_lines if "Zabbix server" in line][0]
    host_get_output = run_script(["host", "get", "--host_id", zabbix_server_id])
    assert "'host': 'Zabbix server'" in host_get_output

def test_create_host():
    run_script(["host", "create", "--ip", "192.168.1.15", "--hostname", "l01t1-zbxa01"])
    host_list_output = run_script(["host", "list"])
    assert 'l01t1-zbxa01' in host_list_output

def test_delete_host():
    host_list_output = run_script(["host", "list"])
    host_list_lines = host_list_output.splitlines()
    created_host_id = [line.split()[0] for line in host_list_lines if "l01t1-zbxa01" in line][0]
    run_script(["host", "delete", "--host_id", created_host_id])
    output_after_delete = run_script(["host", "list"])
    assert 'l01t1-zbxa01' not in output_after_delete

def test_list_items():
    host_list_output = run_script(["host", "list"])
    host_list_lines = host_list_output.splitlines()
    zabbix_server_id = [line.split()[0] for line in host_list_lines if "Zabbix server" in line][0]
    item_list_output = run_script(["item", "list", "--host_id", zabbix_server_id])
    assert "system" in item_list_output.lower()

def test_create_item():
    host_list_output = run_script(["host", "list"])
    host_list_lines = host_list_output.splitlines()
    zabbix_server_id = [line.split()[0] for line in host_list_lines if "Zabbix server" in line][0]
    run_script(["item", "create", "--host_id", zabbix_server_id, "--key", "custom.key", "--name", "Custom Key"])
    item_list_output = run_script(["item", "list", "--host_id", zabbix_server_id])
    assert 'Custom Key' in item_list_output

def test_delete_item():
    host_list_output = run_script(["host", "list"])
    host_list_lines = host_list_output.splitlines()
    zabbix_server_id = [line.split()[0] for line in host_list_lines if "Zabbix server" in line][0]
    item_list_output = run_script(["item", "list", "--host_id", zabbix_server_id])
    item_list_lines = item_list_output.splitlines()
    created_item_id = [line.split()[0] for line in item_list_lines if "Custom Key" in line][0]
    run_script(["item", "delete", "--item_id", created_item_id])
    output_after_delete = run_script(["item", "list", "--host_id", zabbix_server_id])
    assert 'Custom Key' not in output_after_delete

def test_create_trigger():
    expression = "{Zabbix server:system.uptime.last()}=0"
    description = "System Uptime Trigger"
    host_list_output = run_script(["host", "list"])
    host_list_lines = host_list_output.splitlines()
    zabbix_server_id = [line.split()[0] for line in host_list_lines if "Zabbix server" in line][0]
    run_script(["trigger", "create", "--expression", expression, "--description", description])
    trigger_list_output = run_script(["trigger", "list", "--host_id", zabbix_server_id])
    assert description in trigger_list_output

def test_list_triggers():
    description = "System Uptime Trigger"
    host_list_output = run_script(["host", "list"])
    host_list_lines = host_list_output.splitlines()
    zabbix_server_id = [line.split()[0] for line in host_list_lines if "Zabbix server" in line][0]
    trigger_list_output = run_script(["trigger", "list", "--host_id", zabbix_server_id])
    assert description in trigger_list_output

def test_delete_trigger():
    description = "System Uptime Trigger"
    host_list_output = run_script(["host", "list"])
    host_list_lines = host_list_output.splitlines()
    zabbix_server_id = [line.split()[0] for line in host_list_lines if "Zabbix server" in line][0]
    trigger_list_output = run_script(["trigger", "list", "--host_id", zabbix_server_id])
    trigger_list_lines = trigger_list_output.splitlines()
    created_trigger_id = [line.split()[0] for line in trigger_list_lines if description in line][0]
    run_script(["trigger", "delete", "--trigger_id", created_trigger_id])
    output_after_delete = run_script(["trigger", "list", "--host_id", zabbix_server_id])
    assert description not in output_after_delete

def test_list_get_events():
    expression = "{Zabbix server:system.uptime.last()}=0"
    description = "Test Event Trigger"
    run_script(["trigger", "create", "--expression", expression, "--description", description])
    host_list_output = run_script(["host", "list"])
    host_list_lines = host_list_output.splitlines()
    zabbix_server_id = [line.split()[0] for line in host_list_lines if "Zabbix server" in line][0]
    trigger_list_output = run_script(["trigger", "list", "--host_id", zabbix_server_id])
    trigger_list_lines = trigger_list_output.splitlines()
    created_trigger_id = [line.split()[0] for line in trigger_list_lines if description in line][0]
    event_list_output = run_script(["event", "list", "--trigger_id", created_trigger_id])
    event_id = event_list_output.split()[0]
    run_script(["trigger", "delete", "--trigger_id", created_trigger_id])
    event_data = run_script(["event", "get", "--event_id", event_id])
    assert "acknowledged" in event_data

def test_list_alerts():
    alert_list_output = run_script(["alert", "list"])
    assert "alert" in alert_list_output.lower()

def test_get_alert_details():
    alert_list_output = run_script(["alert", "list"])
    if alert_list_output == "No recent alerts.":
        assert True
    else:
        first_alert_id = alert_list_output.splitlines()[0].split()[0]
        alert_get_output = run_script(["alert", "get", "--alert_id", first_alert_id])
        assert "Alert detail" in alert_get_output

def test_list_graphs():
    host_list_output = run_script(["host", "list"])
    host_list_lines = host_list_output.splitlines()
    zabbix_server_id = [line.split()[0] for line in host_list_lines if "Zabbix server" in line][0]
    graph_list_output = run_script(["graph", "list", "--host_id", zabbix_server_id])
    assert "System load" in graph_list_output

# def test_create_graph():
#     item_key = "custom.system.uptime"
#     item_name = "System Uptime Custom"
#     graph_name = "Test Graph"
#     host_list_output = run_script(["host", "list"])
#     host_list_lines = host_list_output.splitlines()
#     zabbix_server_id = [line.split()[0] for line in host_list_lines if "Zabbix server" in line][0]
#     run_script(["item", "create", "--host_id", zabbix_server_id, "--key", item_key, "--name", item_name])
#     item_list_output = run_script(["item", "list", "--host_id", zabbix_server_id])
#     item_id = [line.split()[0] for line in item_list_output.splitlines() if item_name in line][0]
#     run_script(["graph", "create", "--name", graph_name, "--items", f"{item_id}:0:1"])
#     graph_list_output_after = run_script(["graph", "list", "--host_id", zabbix_server_id])
#     item_list_output = run_script(["item", "list", "--host_id", zabbix_server_id])
#     item_list_lines = item_list_output.splitlines()
#     created_item_id = [line.split()[0] for line in item_list_lines if item_name in line][0]
#     run_script(["item", "delete", "--item_id", created_item_id])
#     assert graph_name in graph_list_output_after

# def test_delete_graph():
#     graph_name = "Test Graph"
#     host_list_output = run_script(["host", "list"])
#     host_list_lines = host_list_output.splitlines()
#     zabbix_server_id = [line.split()[0] for line in host_list_lines if "Zabbix server" in line][0]
#     graph_list_output = run_script(["graph", "list", "--host_id", zabbix_server_id])
#     graph_id = [line.split()[0] for line in graph_list_output.splitlines() if graph_name in line][0]
#     run_script(["graph", "delete", "--graph_id", graph_id])
#     output_after_delete = run_script(["graph", "list", "--host_id", zabbix_server_id])
#     assert graph_name not in output_after_delete

def test_list_users():
    user_list_output = run_script(["user", "list"])
    assert "Admin" in user_list_output

def test_create_user():
    user_groups = "8"  # Assuming '8' is the ID for Zabbix administrators. Adjust accordingly.
    run_script(["user", "create", "--alias", "testuser", "--passwd", "test1234", "--usrgrps", user_groups])
    user_list_output = run_script(["user", "list"])
    assert "testuser" in user_list_output

def test_get_user_details():
    user_list_output = run_script(["user", "list"])
    if "testuser" not in user_list_output:
        pytest.skip("testuser not found, skipping this test")
    testuser_id = [line.split()[0] for line in user_list_output.splitlines() if "testuser" in line][0]
    user_get_output = run_script(["user", "get", "--user_id", testuser_id])
    assert "testuser" in user_get_output

def test_delete_user():
    user_list_output = run_script(["user", "list"])
    if "testuser" not in user_list_output:
        pytest.skip("testuser not found, skipping this test")
    testuser_id = [line.split()[0] for line in user_list_output.splitlines() if "testuser" in line][0]
    run_script(["user", "delete", "--user_id", testuser_id])
    updated_list = run_script(["user", "list"])
    assert "testuser" not in updated_list

def test_create_group():
    run_script(["group", "create", "--name", "Test_1_Group"])
    group_list_output = run_script(["group", "list"])
    assert 'Test_1_Group' in group_list_output

def test_get_group_details():
    group_list_output = run_script(["group", "list"])
    testgroup_id = [line.split()[0] for line in group_list_output.splitlines() if "Test_1_Group" in line][0]
    group_details = run_script(["group", "get", "--group_id", testgroup_id])
    assert "'name': 'Test_1_Group'" in group_details

def test_delete_group():
    group_list_output = run_script(["group", "list"])
    testgroup_id = [line.split()[0] for line in group_list_output.splitlines() if "Test_1_Group" in line][0]
    run_script(["group", "delete", "--group_id", testgroup_id])
    group_list_output_after = run_script(["group", "list"])
    assert 'Test_1_Group' not in group_list_output_after

def test_list_groups():
    run_script(["group", "create", "--name", "Test_2_Group"])
    group_list_output = run_script(["group", "list"])
    assert "Test_2_Group" in group_list_output
    testgroup_id = [line.split()[0] for line in group_list_output.splitlines() if "Test_2_Group" in line][0]
    run_script(["group", "delete", "--group_id", testgroup_id])

def test_create_template():
    run_script(["group", "create", "--name", "Template_1_TestGroup"])
    group_list_output = run_script(["group", "list"])
    testgroup_id = [line.split()[0] for line in group_list_output.splitlines() if "Template_1_TestGroup" in line][0]
    run_script(["template", "create", "--name", "Test_1_Template", "--group_ids", testgroup_id])
    template_list_output = run_script(["template", "list"])
    assert 'Test_1_Template' in template_list_output

def test_get_template_details():
    template_list_output = run_script(["template", "list"])
    testtemplate_id = [line.split()[0] for line in template_list_output.splitlines() if "Test_1_Template" in line][0]
    template_details = run_script(["template", "get", "--template_id", testtemplate_id])
    assert "'host': 'Test_1_Template'" in template_details

def test_list_templates():
    template_list_output = run_script(["template", "list"])
    assert "Test_1_Template" in template_list_output

def test_delete_template():
    template_list_output = run_script(["template", "list"])
    testtemplate_id = [line.split()[0] for line in template_list_output.splitlines() if "Test_1_Template" in line][0]
    run_script(["template", "delete", "--template_id", testtemplate_id])
    group_list_output = run_script(["group", "list"])
    testgroup_id = [line.split()[0] for line in group_list_output.splitlines() if "Template_1_TestGroup" in line][0]
    run_script(["group", "delete", "--group_id", testgroup_id])
    template_list_output_after = run_script(["template", "list"])
    assert 'Test_1_Template' not in template_list_output_after

def test_start_maintenance():
    host_list_output = run_script(["host", "list"])
    host_list_lines = host_list_output.splitlines()
    zabbix_server_id = [line.split()[0] for line in host_list_lines if "Zabbix server" in line][0]
    run_script(["maintenance", "start", "--name", "TestMaintenance", "--host_ids", zabbix_server_id, "--active_since", "1640995200", "--active_till", "1641002400"])
    maintenance_list_output = run_script(["maintenance", "list"])
    assert "TestMaintenance" in maintenance_list_output

def test_list_maintenances():
    maintenance_list_output = run_script(["maintenance", "list"])
    assert "TestMaintenance" in maintenance_list_output

def test_end_maintenance():
    maintenance_list_output = run_script(["maintenance", "list"])
    testmaintenance_id = [line.split()[0] for line in maintenance_list_output.splitlines() if "TestMaintenance" in line][0]
    run_script(["maintenance", "end", "--maintenance_id", testmaintenance_id])
    maintenance_list_output_after = run_script(["maintenance", "list"])
    assert "TestMaintenance" not in maintenance_list_output_after
